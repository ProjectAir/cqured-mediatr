<?php
namespace Cqured\MediatR;

/**
 * Marker interface to represent a notification
 */
interface INotification
{}
