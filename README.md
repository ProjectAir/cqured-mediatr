MediatR
=======

<!-- [![Build Status](https://ci.appveyor.com/api/projects/status/github/jbogard/mediatr?branch=master&svg=true)](https://ci.appveyor.com/project/jbogard/mediatr) 
[![NuGet](https://img.shields.io/nuget/dt/mediatr.svg)](https://www.nuget.org/packages/mediatr) 
[![Packagist](https://img.shields.io/nuget/vpre/mediatr.svg)](https://www.nuget.org/packages/mediatr) -->

Simple mediator implementation in PHP

In-process messaging with no dependencies.

Supports request/response, commands, queries, notifications and events, synchronous and async with intelligent dispatching via PHP Swoole.

Examples in the [wiki](https://github.com/jbogard/MediatR/wiki).

### Installing MediatR

You should install [MediatR with Composer](https://www.nuget.org/packages/MediatR):

    composer require cqured/mediatr
    

